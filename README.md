
# Tune Imitator

This software uses the structure of a given melody to generate new melodies.

For example, this is Bach's [Musical Offering - BWV 1079, Canon perpetuus super thema regium](1079-02.flac) while this is the
[music generated](cuasi_1079-02.flac) from its structure by this program.

This could be a [second movement](cuasi_moonlight.flac) of Beethoven's
[Moonlight sonata](piano_moonlight.flac). Just kidding.

## Melody network

Melodies are sequences of notes. To extract a graph from a melody, I
use note intervals as edges, and pitches as nodes.

The result is a network such as this one:

![example pitch graphs](example_pitch_graph.png)

The [NetworkX](https://networkx.org/) library allows me to use nodes
to store a list of every note that plays that pitch. Notes are note
objects as defined by the [music21](http://web.mit.edu/music21/)
library. They include volume, duration and other attributes. These
details are not shown in the visualization above, to keep it simple.

## Random walk

This network is the input for a random walk algorithm. It starts in
the same note as the original music, i.e. in the same node. It adds
that note to the score, then randomly chooses a connected neighbor
node and adds that note to the score, and reiterates for as many notes
as the original score had.

Notes include rests, and information about duration and volume.

The result is a new melody that sounds very similar to the original
one, but different. Ofcourse there is a huge difference between the
Bach walk and the random walk! But still it's interesting how the
network captures so much of the original musical structure.



