from music21 import converter, instrument, note, stream
import networkx as nx
import argparse
import pickle

parser = argparse.ArgumentParser(
    description="create pickle with graph from midi and metadata")

parser.add_argument('--midi',
                    type=argparse.FileType('r'),
                    required=True,
                    help='a midi file')

parser.add_argument('--pickle',
                    type=argparse.FileType('wb'),
                    required=True,
                    help='output pickle')

args = parser.parse_args()

tune = converter.parse(args.midi.name)
tune = tune.voicesToParts()
graphs = []


for i, part in enumerate(tune.elements):
    G = nx.DiGraph()
    steps = 0

    extras = {}
    start_note = True
    pitch = None
    for j, n in enumerate(part):
        last_pitch = pitch

        # pitch is going to be node id
        if type(n) == note.Note:
            pitch = "%s%s" % (n.name, n.octave)
        elif type(n) == note.Rest:
            pitch = "rest"
        else:
            extras[j]=n            
            pitch = None

        # ignore items in part that are not notes nor rests
        if pitch is not None:

            # nodes in graph contain a list of note objects
            notes = G.nodes.get(pitch,
                                {'notes': []})['notes']
            notes.append(n)

            if start_note:
                G.add_node(pitch,
                           notes=notes,
                           start_note=n)
                start_note = False
            else:
                G.add_node(pitch,
                           notes=notes)


        # edges are created from note intervals,
        # or note-to-rest interactions
        if last_pitch is not None and pitch is not None:

            # reiterations of intervals increase edge weight
            w = G.get_edge_data(last_pitch,
                                pitch,
                                default={'w': 0})['w']
            w += 1

            G.add_edge(last_pitch,
                       pitch,
                       w=w)

            # keep number of steps for random walker
            steps += 1

    graphs.append((G, steps, extras))

pickle.dump(graphs,
            args.pickle)

