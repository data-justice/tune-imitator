

# Cómo correr este programa en Windows

1. Descargar e instalar [Python](https://www.python.org/).

2. Clona o descarga este repositorio.

  **Para clonar**: hay que usar git que puede descargarse [aquí](https://git-scm.com/download/win).
  Luego, desde PowerShell este comando:
  
```
  PS > git clone https://gitlab.com/data-justice/tune-imitator.git
```

  Alternativamente, **descargar** este archivo, descomprimirlo:
  <https://gitlab.com/data-justice/tune-imitator/-/archive/master/tune-imitator-master.zip>

3. Preparar el shell para permitir ejecución de scripts.

   Para deshabilitar el marco de seguridad, y que cualquier script pueda ejecutarse:

```
   PS > Set-ExecutionPolicy -ExecutionPolicy Bypass
```

   [Este documento](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.1) describe los permisos de PowerShell. Recomiendo leerlo y entenderlo. Deshabilitar todo el marco  puede no ser la mejor opción para todos los usuarios.

4. Instalar las bibliotecas de Python: crear un ambiente virtual, activarlo, y luego usar pip.
   
   ```
   PS > cd tune-imitator
   PS > python.exe -m venv venv
   PS > venv/Scripts/Activate.ps1
   (venv) PS > pip install -r requirements.txt
   [... se instalan las bibliotecas]
  ```

5. Crear un grafo a partir de un archivo MIDI, guardarlo en el formato *pickle*. En este ejemplo uso el archivo 1079-02.mid.
```
   (venv) PS > python.exe tune2graph.py --midi 1079-02.mid --pickle 1079-02.pickle
```

6. Crear nuevos archivos MIDI a partir del grafo.
```
   (venv) PS > python.exe gpickle2midi.py --pickle 1079-02.pickle --midi 1079-02_alternativo.mid
```
