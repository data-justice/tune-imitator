# Caminante aleatorio

Una melodía es una sucesión lineal de tonalidades que el escucha
percibe como una sóla entidad. Es una combinación de tono y
ritmo. Podría decirse que una melodía es la forma que se destaca sobre
el fondo que es el acompañamiento musical de una pieza.

Cualquier par de tonos en esa suceción crea un intervalo. Es la unidad
mínima de interacción entre dos tonalidades en la sucesión que
conforma una melodía.

<figura de pentagrama, con beams conectando octavas, panel 2 el grafo extraido>

![intervalos como conexiones de la red](interval_network.png)

El conjunto de esas interacciones crea una estructura, una red en la
que los nodos son tonalidades y las conexiones son intervalos. Esa red
es un modelo de la melodía.

Tomar cada intervalo por separado es análisis, recuperarlos en una
sóla red es síntesis. Un modelo es una representación sintética de un
fenómeno.

Las redes complejas son una forma de modelar muy eficaz cuando el
fenómeno bajo estudio se puede estudiar descomponiéndolo en
interacciones. Se usan redes para estudiar todo tipo de fenómenos:
interacciones simbióticas o tróficas en una red ecológica, etc. 

Este experimiento 

Las estructuras son manifestaciones de procesos. Procesos son
sostenidos por estructuras. Causalidad circular.

Proceso ocurre en el tiempo, en el fenómeno que es una pieza musical
es el orden preciso en el que ocurre la sucesión de notas, el orden de
los intervalos. La estructura subyacente es el mapa de estas
interacciones, una red de tonalidades conectadas entre sí por sonar
una después de otra.

Escribí un programa para generar esos modelos. Lee notación musical
digitalizada. Descompone la melodía de cada instrumento en sus
intervalos, por cada intervalo crea una conexión en la red, uniendo
dos notas.

Se obtiene una red dirigida como esta:

![example pitch graphs](example_pitch_graph.png)

Una red dirigida es un tipo de red compleja. Una red compleja es un
tipo de modelo computacional. Un modelo computacional es un tipo de
modelo, es una representación sintética de un fenómeno.



The [NetworkX](https://networkx.org/) library allows me to use nodes
to store a list of every note that plays that pitch. Notes are note
objects as defined by the [music21](http://web.mit.edu/music21/)
library. They include volume, duration and other attributes. These
details are not shown in the visualization above, to keep it simple.

## Caminante aleatorio

This network is the input for a random walk algorithm. It starts in
the same note as the original music, i.e. in the same node. It adds
that note to the score, then randomly chooses a connected neighbor
node and adds that note to the score, and reiterates for as many notes
as the original score had.

Notes include rests, and information about duration and volume.

The result is a new melody that sounds very similar to the original
one, but different. Ofcourse there is a huge difference between the
Bach walk and the random walk! But still it's interesting how the
network captures so much of the original musical structure.
