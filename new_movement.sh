#!/bin/bash


BASE=`basename $1 .mid`
PICKLE=${BASE}.pickle

if ! test -e "$PICKLE"; then
    echo "creating pickle from midi"
    python tune2graph.py --midi $1 --pickle $PICKLE
fi


CUASI=cuasi_${BASE}.mid
python gpickle2midi.py --pickle $PICKLE --midi $CUASI

timidity $CUASI
