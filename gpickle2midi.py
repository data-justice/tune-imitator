import argparse
import networkx as nx
from music21 import note, stream
import copy
import random
import pickle

parser = argparse.ArgumentParser(
    description="use graph and metadata to random-walk new melodies")

parser.add_argument('--pickle',
                    type=argparse.FileType('rb'),
                    help='a pickled melody graph')

parser.add_argument('--midi',
                    type=argparse.FileType('wb'),
                    help='output midi file')

args = parser.parse_args()


def random_walk_part(g, steps, extras):

    part = stream.Part()

    # random walk
    i = 0
    j = 0
    while i < steps:
        
        if extras.get(j, None):
            part.append(extras[j])
            j += 1
            continue
        
        if i == 0:
            for v in g.nodes:
                if 'start_note' in g.nodes[v]:
                    n = copy.deepcopy(g.nodes[v]['start_note'])
                    s = v
        else:
            n = copy.deepcopy(random.choice(g.nodes[s]['notes']))

        part.append(n)
        i += 1

        paths = []
        for v in g.neighbors(s):
            w = g.get_edge_data(s, v)['w']
            paths += [v,] * w

        if paths:
            s = random.choice(paths)
        else:
            s = random.choice(list(g.nodes))

    return part


graphs = pickle.load(args.pickle)

out_stream = stream.Stream()

for (g, steps, extras) in graphs:
    out_stream.append(random_walk_part(g, steps, extras))

out_stream.write('midi',
                 args.midi.name)
